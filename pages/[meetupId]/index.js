import MeetupDetail from "../../components/meetups/MeetupDetail";

function MeetupDetails() {
	return (
		<MeetupDetail
			image="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Val%C3%A8re_et_Haut_de_Cry.jpg/1280px-Val%C3%A8re_et_Haut_de_Cry.jpg"
			title="A First Meetup"
			address="Some Address 123"
			description="First Meetup"
		/>
	);
}

export async function getStaticPaths() {
	return {
		fallback: false,
		paths: [
			{
				params: {
					meetupId: "m1",
				},
			},
			{
				params: {
					meetupId: "m2",
				},
			},
		],
	};
}

export async function getStaticProps(context) {
	const meetupId = context.params.meetupId;

	return {
		props: {
			meetupData: {
				id: meetupId,
				image:
					"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Val%C3%A8re_et_Haut_de_Cry.jpg/1280px-Val%C3%A8re_et_Haut_de_Cry.jpg",
				title: "A First Meetup",
				address: "Some Address 123",
				description: "First Meetup",
			},
		},
	};
}

export default MeetupDetails;
