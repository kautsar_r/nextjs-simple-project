import MeetupList from "../components/meetups/MeetupList";

const DUMMY_MEETUPS = [
	{
		id: "m1",
		title: "A First Meetup",
		image:
			"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Val%C3%A8re_et_Haut_de_Cry.jpg/1280px-Val%C3%A8re_et_Haut_de_Cry.jpg",
		address: "Some Address 123",
		description: "First Meetup",
	},
	{
		id: "m2",
		title: "A Second Meetup",
		image:
			"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Val%C3%A8re_et_Haut_de_Cry.jpg/1280px-Val%C3%A8re_et_Haut_de_Cry.jpg",
		address: "Some Address 456",
		description: "Second Meetup",
	},
];

function HomePage(props) {
	return <MeetupList meetups={props.meetups} />;
}

// export function getServerSideProps(context) {
// 	const req = context.req;
// 	const res = context.res;

// 	return {
// 		props: {
// 			meetups: DUMMY_MEETUPS,
// 		},
// 	};
// }

export async function getStaticProps() {
	return {
		props: {
			meetups: DUMMY_MEETUPS,
		},
		revalidate: 1,
	};
}

export default HomePage;
